#!/bin/sh

curl --request POST \
     --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
     --data name="dnslease_$CI_COMMIT_REF_NAME" \
     --data url="https://gitlab.com/mikroscript/dnslease/-/jobs/$CI_JOB_ID/artifacts/download" \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_REF_NAME/assets/links"