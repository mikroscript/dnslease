# Installation

Download the [latest release](https://gitlab.com/mikroscript/dnslease/-/releases) and extract it.

Copy the `flash` folder to the root of your router's files directory.
The files should be in `flash/mikroscript/dnslease`.

Run the following command from a terminal and follow the instructions to set up DNSLease for a DHCP server. You may run the command multiple times to install the script for multiple servers.

```
/import flash/mikroscript/dnslease/install.rsc
```

# Update

To update, remove the `flash/mikroscript/dnslease` folder, download and extract the latest release, and copy the `flash` folder to the root of your device the same as you installed. You shouldn't have to reconfigure the script for each DHCP server unless the major version has changed.

# Uninstall

Run the following command to uninstall DNSLease from a DHCP server.
```
/import flash/mikroscript/dnslease/uninstall.rsc
```

After uninstalling DNSLease from all DHCP servers, you may delete the `flash/mikroscript/dnslease` folder if you wish.