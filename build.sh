#!/bin/sh

mkdir -p ./dnslease_$CI_COMMIT_REF_NAME/flash/mikroscript/dnslease/lib/

cp ./helpers/src/** ./dnslease_$CI_COMMIT_REF_NAME/flash/mikroscript/dnslease/lib/
cp -r ./src/** ./dnslease_$CI_COMMIT_REF_NAME/flash/mikroscript/dnslease/