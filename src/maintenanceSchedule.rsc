{
  :local serverName "SERVER_NAME";
  :local dnsDomain "DNS_DOMAIN";
  :local useComments "USE_COMMENTS";
  :local dnsTtl "DNS_TTL";
  
  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local maintenance [$import "maintenance"];

  $maintenance \
    dnsDomain=$dnsDomain \
    useComments=$useComments \
    dnsTtl=$dnsTtl \
    serverName=$serverName;
}
