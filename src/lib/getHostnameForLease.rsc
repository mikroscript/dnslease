{
  # param: lease
  # param: useComments
  # returns: hostname

  :local result ($lease -> "host-name");

  :if ($useComments = "y" && [:len ($lease -> "comment")] > 0) do={
    :set result ($lease -> "comment");
  }
  
  :return $result;
}