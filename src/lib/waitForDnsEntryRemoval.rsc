{
  # param: dnsName

  :local delayed false;
  :while ([:len [/ip dns static find where name=$dnsName]] > 0) do={
    :if ($delayed = false) do={
      :log info "Waiting for dns entry with name '$dnsName' to be removed...";

      :set delayed true;
    }
  }
}