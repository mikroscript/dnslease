{
  # param: dnsEntry
  # param: serverName
  # returns: the mac address

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local replaceString [$import "lib/replaceString"];

  :if ([:len ($dnsEntry -> "comment")] > 0) do={
    :local data [$replaceString in=($dnsEntry -> "comment") replace=("dnslease-auto-" . $serverName ." ") with=""];
    :local result [:toarray $data];

    :return ($result -> 0);
  }
}