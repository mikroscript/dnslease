{
  # param: dnsName

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local waitForDnsEntryRemoval [$import "lib/waitForDnsEntryRemoval"];

  :if ([:len [/ip dns static find where name=$dnsName]] > 0) do={
    /ip dns static remove [/ip dns static find where name=$dnsName];
    $waitForDnsEntryRemoval dnsName=$dnsName;
  }
}