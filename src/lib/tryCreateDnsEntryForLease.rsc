{
  # param: lease
  # param: leaseHostname
  # param: dnsDomain
  # param: dnsTtl

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local getCommentForDnsEntry [$import "lib/getCommentForDnsEntry"];
  :local getDnsSubdomainFromHostname [$import "lib/getDnsSubdomainFromHostname"];

  :local dnsSubdomain [$getDnsSubdomainFromHostname hostname=$leaseHostname];
  :local dnsName "$dnsSubdomain.$dnsDomain";
  :local dnsComment [$getCommentForDnsEntry lease=$lease];
  
  :if ([:len [/ip dns static find where name=$dnsName]] = 0) do={
    # No entry for this lease. Create one.
    :log info "DNSLease: Creating DNS entry for '$dnsName'.";

    /ip dns static add \
      name=$dnsName address=($lease -> "active-address") \
      ttl=$dnsTtl comment=$dnsComment;
  }
}