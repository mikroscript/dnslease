{
  # param: lease
  # param: leaseHostname
  # param: dnsDomain

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local getCommentForDnsEntry [$import "lib/getCommentForDnsEntry"];
  :local getDnsSubdomainFromHostname [$import "lib/getDnsSubdomainFromHostname"];
  :local removeDnsEntryAndWait [$import "lib/removeDnsEntryAndWait"];
  :local replaceString [$import "lib/replaceString"];

  :local dnsSubdomain [$getDnsSubdomainFromHostname hostname=$leaseHostname];
  :local dnsName "$dnsSubdomain.$dnsDomain";
  :local dnsEntry [:pick [/ip dns static print as-value detail where name=$dnsName] 0];

  :if (($dnsEntry -> "comment") ~ "^dnslease-auto") do={
    :local dnsComment [$getCommentForDnsEntry lease=$lease];
    :set dnsComment [$replaceString in=$dnsComment replace=("dnslease-auto-" . ($lease -> "server") . " ") with=""];

    :if (($dnsEntry -> "comment") ~ "$dnsComment\$") do={
      # Comment matches. Make sure IP address matches, too.
      :if (($dnsEntry -> "address") != ($lease -> "active-address")) do={
        :log info "DNSLease: IP mismatch for '$dnsName'.";
        
        $removeDnsEntryAndWait dnsName=$dnsName;
      }
    } else={
      :log info "DNSLease: Lease changed for '$dnsName'.";

      $removeDnsEntryAndWait dnsName=$dnsName;
    }
  }
}