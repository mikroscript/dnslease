{
  # param: hostname

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local replaceChar [$import "lib/replaceChar"];
  :local lowerCase [$import "lib/lowerCase"];
  :local stripInvalidDomainChars [$import "lib/stripInvalidDomainChars"];

  :local result $hostname;

  :set result [$replaceChar in=$result replace="-" with="."];
  :set result [$lowerCase in=$result];
  :set result [$stripInvalidDomainChars in=$result];

  :return $result;
}