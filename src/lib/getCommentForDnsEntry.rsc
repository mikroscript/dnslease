{
  # input: lease
  # returns: comment ; string

  :local prefix ("dnslease-auto-" . ($lease -> "server"));
  :local macAddress ("\"" . ($lease -> "mac-address") . "\"");
  :local clientId ("\"" . ($lease -> "client-id") . "\"");

  :return "$prefix $macAddress,$clientId"
}