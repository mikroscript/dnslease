{
  #param: in

  :local allowedChars "abcdefghijklmnopqrstuvwxyz0123456789.";
  :local numChars [:len $in];
  :local result "";

  :if ($numChars > 63) do={
    :set numChars 63;
  };

  :for i from=0 to=($numChars - 1) do={

    :local char [:pick $in $i];

    :if ([:find $allowedChars $char] < 0) do={
      :set char "";
    }

    :set result ($result . $char);
  }

  :return $result;
}