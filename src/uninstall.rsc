{
  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local readLine [$import "lib/readLine"];

  :put "Enter DHCP server name";
  :local serverName [$readLine];
  :while ([:len [/ip dhcp-server find name=$serverName]] = 0) do={
    :put "No DHCP server found with that name.";
    :put "Either create it first or enter a different name.";

    :set serverName [$readLine];
  }

  :local removeDnsEntries "";
  :while ($removeDnsEntries != "y" && $removeDnsEntries != "n") do={
    :put "Would you like to remove existing DNS entries for this server? (y/n)";

    :set $removeDnsEntries [$readLine];
  }

  :put "Removing lease-script from $serverName";
  /ip dhcp-server set $serverName lease-script="";

  :put "Removing maintenance script for $serverName from schedule";
  /system scheduler remove [/system scheduler find name="dnslease_maintenance_$serverName"];

  :if ($removeDnsEntries = "y") do={
    :put "Clearing DNS entries for $serverName";
    /ip dns static remove [/ip dns static find comment~"^dnslease-auto-$serverName"];
  }
}