{
  # param: dnsDomain
  # param: useComments
  # param: dnsTtl
  # param: leaseBound
  # param: leaseServerName
  # param: leaseActMAC
  # param: leaseActIP
  # param: leaseHostname

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  # Import libraries
  :local removeInvalidDnsEntryForLease [$import "lib/removeInvalidDnsEntryForLease"];
  :local tryCreateDnsEntryForLease [$import "lib/tryCreateDnsEntryForLease"];

  # Code

  # Don't handle lease unbound.
  # The DNS entry will be replaced by a new one
  # if the lease for it is bound again.
  # Otherwise, the maintenance script will remove the
  # entry next time it runs.
  # This simlifies this script by making it
  # effectively only run once for a reconnection.
  :if ($leaseBound = "1") do={
    # Lease bound
    :local leaseComment ([:pick \
      [/ip dhcp-server lease print as-value where \
        status="bound" server=$leaseServerName \
        mac-address=$leaseActMAC address=$leaseActIP] 0] \
          -> "comment");

    :local chosenHostname $leaseHostname;
    # Set hostname from comment if enabled and available
    :if ($useComments = "y" && [:len $leaseComment] > 0) do={
      :set chosenHostname $leaseComment;
    }

    :if ([:len $chosenHostname] > 0) do={
      # Get lease from available information
      :local lease [:pick \
        [/ip dhcp-server lease print as-value detail where \
          status="bound" \
          server=$leaseServerName \
          active-mac-address=$leaseActMAC \
          active-address=$leaseActIP] 0];

      $removeInvalidDnsEntryForLease lease=$lease leaseHostname=$chosenHostname dnsDomain=$dnsDomain;

      $tryCreateDnsEntryForLease lease=$lease leaseHostname=$chosenHostname dnsDomain=$dnsDomain dnsTtl=$dnsTtl;
    }
  }
}