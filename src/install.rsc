{
  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local readLine [$import "lib/readLine"];
  :local replaceString [$import "lib/replaceString"];

  :put "Enter DHCP server name";
  :local serverName [$readLine];
  :while ([:len [/ip dhcp-server find name=$serverName]] = 0) do={
    :put "No DHCP server found with that name.";
    :put "Either create it first or enter a different name.";

    :set serverName [$readLine];
  }

  :local dnsDomain "";
  :while ([:len $dnsDomain] = 0) do={
    :put "Enter DNS domain to use for this DHCP server.";
    :put "Example: lan.example.com";
    :set dnsDomain [$readLine];
  }

  :local dnsTtl "";
  :while ([:len $dnsTtl] = 0) do={
    :put "Enter DNS TTL. '00:10:00' is a good default.";
    :set dnsTtl [$readLine];
  }

  :local useComments "";
  :while ($useComments != "y" && $useComments != "n") do={
    :put "Would you like to get hostname from DHCP lease comment? (y/n)";

    :set useComments [$readLine];
  }

  :local leaseScript [/file get "flash/mikroscript/dnslease/leaseScript.rsc" contents];
  :set leaseScript [$replaceString in=$leaseScript replace="DNS_DOMAIN" with=$dnsDomain];
  :set leaseScript [$replaceString in=$leaseScript replace="USE_COMMENTS" with=$useComments];
  :set leaseScript [$replaceString in=$leaseScript replace="DNS_TTL" with=$dnsTtl];

  :put "Setting lease-script for $serverName";
  /ip dhcp-server set $serverName lease-script=$leaseScript;

  :local maintenanceScript [/file get "flash/mikroscript/dnslease/maintenanceSchedule.rsc" contents];
  :set maintenanceScript [$replaceString in=$maintenanceScript replace="SERVER_NAME" with=$serverName];
  :set maintenanceScript [$replaceString in=$maintenanceScript replace="DNS_DOMAIN" with=$dnsDomain];
  :set maintenanceScript [$replaceString in=$maintenanceScript replace="USE_COMMENTS" with=$useComments];
  :set maintenanceScript [$replaceString in=$maintenanceScript replace="DNS_TTL" with=$dnsTtl];

  :put "Adding maintenance script for $serverName to schedule";
  /system scheduler add name="dnslease_maintenance_$serverName" policy=read,write start-time=startup interval="00:10:00" on-event=$maintenanceScript;

  :put "Running maintenance script";
  :local maintenance [:parse [/file get "flash/mikroscript/dnslease/maintenance.rsc" contents]];
  $maintenance serverName=$serverName dnsDomain=$dnsDomain useComments=$useComments dnsTtl=$dnsTtl;
}