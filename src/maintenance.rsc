{
  # param: serverName
  # param: dnsDomain
  # param: useComments
  # param: dnsTtl

  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  # Load helper methods
  :local getClientIdForDnsEntry [$import "lib/getClientIdForDnsEntry"];
  :local getHostnameForLease [$import "lib/getHostnameForLease"];
  :local getMacAddressForDnsEntry [$import "lib/getMacAddressForDnsEntry"];
  :local removeDnsEntryAndWait [$import "lib/removeDnsEntryAndWait"];
  :local removeInvalidDnsEntryForLease [$import "lib/removeInvalidDnsEntryForLease"];
  :local tryCreateDnsEntryForLease [$import "lib/tryCreateDnsEntryForLease"];

  # Process existing DNS entries
  :local dnsEntries [/ip dns static print as-value where comment~"^dnslease-auto-$serverName"];
  :foreach dnsEntry in=$dnsEntries do={
    :local entryClientId [$getClientIdForDnsEntry dnsEntry=$dnsEntry serverName=$serverName];
    :local entryMacAddress [$getMacAddressForDnsEntry dnsEntry=$dnsEntry serverName=$serverName];
    :local lease [:pick [/ip dhcp-server lease print as-value detail where \
      status="bound" server=$serverName \
      mac-address=$entryMacAddress \
      client-id=$entryClientId] 0];

    :if ([:len $lease] = 0) do={
      :log info ("DNSLease: DHCP lease no longer exists for '" . ($dnsEntry -> "name") . "'.");

      $removeDnsEntryAndWait dnsName=($dnsEntry -> "name");
    } else={
      # DHCP lease found. Make sure DNS entry matches.
      
      :local leaseHostname [$getHostnameForLease lease=$lease useComments=$useComments];
      $removeInvalidDnsEntryForLease lease=$lease leaseHostname=$leaseHostname dnsDomain=$dnsDomain;
    }
  }

  # Add any missing DNS entries
  :local leases [/ip dhcp-server lease print as-value detail where server="$serverName" status="bound"];
  :foreach lease in=$leases do={
    :local leaseHostname [$getHostnameForLease lease=$lease useComments=$useComments];

    :if ([:len $leaseHostname] > 0) do={
      $tryCreateDnsEntryForLease lease=$lease leaseHostname=$leaseHostname dnsDomain=$dnsDomain dnsTtl=$dnsTtl;
    }
  }
}
