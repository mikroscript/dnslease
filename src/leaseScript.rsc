# DNSLease

{
  :local dnsDomain "DNS_DOMAIN";
  :local useComments "USE_COMMENTS";
  :local dnsTtl "DNS_TTL";
  
  :local import do={
    # param: $1 ; library name
    # returns: parsed function
    :local workingDir "flash/mikroscript/dnslease/";

    :return [:parse [/file get ($workingDir . $1 . ".rsc") contents]];
  }

  :local onLease [$import "onLease"];

  $onLease \
    dnsDomain=$dnsDomain \
    useComments=$useComments \
    dnsTtl=$dnsTtl \
    leaseBound=$leaseBound \
    leaseServerName=$leaseServerName \
    leaseActMAC=$leaseActMAC \
    leaseActIP=$leaseActIP \
    leaseHostname=$"lease-hostname";
}
